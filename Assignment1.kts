
class SimpleSpice {
    var name:String = "curry"
    var spiciness:String = "mild"
    var heat : Int = 0
        get() {
            return if (spiciness == "nice") {
                10
            } else if (spiciness == "mild") {
                5
            } else {
                0
            }
        }
}


val objectOfSimpleSpice = SimpleSpice()
println(objectOfSimpleSpice.name)
println(objectOfSimpleSpice.spiciness)
println(objectOfSimpleSpice.heat)
objectOfSimpleSpice.spiciness = "nice"
println(objectOfSimpleSpice.heat)
